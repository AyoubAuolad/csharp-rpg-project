# RPG CHARACTER GAME

For the assignment of module 4 I had to develop a windows console application. This application had to be a rpg styled game with all the basic functions of a RPG styled game.
The 2 biggest subject are the characters and the items. By leveling your character up your choice of items grows. These items are stronger and more powerful the higher the level requirement is.
Keep training to level up and get the best weapons and armor in game!

## TABLE OF CONTENTS
- Install
- Usage
- Maintainers
- Contributers
- License

## INSTALL
1. Clone or download a zip 

## USAGE
- Register with a username and get redirected to the main menu.
- Navigate through the menu to start your adventure.
- Train to level up your character.
- Buy items from the equipment store.
- Equip weapons and armor.

## MAINTAINERS 
@ayoub-auolad-ali

## CONTRIBUTERS
[link](https://github.com/RichardLitt/standard-readme)
<a href="https://github.com/RichardLitt/standard-readme">RICHARD TLS READ ME TEMPLATE</a>

## LICENSE
Copyright (c) 2012-2022 Scott Chacon and others

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
