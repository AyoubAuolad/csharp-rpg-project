﻿using System;
using System.Collections.Generic;
using RPG_CONSOLE_APP;
using RPG_CONSOLE_APP.Equipment;
using RPG_CONSOLE_APP.Equipment.Exceptions;

namespace RPG_CONSOLE_APP
{
    public class Program
    {
        public static void Main(string[] args)
        {
            
            MainMenu(CreateCharacter());
            //Dictionary of - int key, item value
        }

        /// <summary>
        /// Creates a character of chosen class with base attributes
        /// </summary>
        /// <returns>Character</returns>
        public static Character CreateCharacter() {

            Console.WriteLine("What's your name?");
            string username = Console.ReadLine();
            Console.WriteLine("What kind character you want to be: (MA) 'Magician' | (RA) 'Ranger'| (WA) 'Warrior' | (RO) Rogue ? ");
            string accounttype = Console.ReadLine().ToUpper();

            switch (accounttype) {

                case "MA":
                    Mage mage = new Mage(username, 1, 1, 1, 8, "Mage", 1);
                    return mage;

                case "RA":
                    Ranger range = new Ranger(username, 1, 1, 4, 4, "Ranger", 1);
                    return range;

                case "WA":
                    Warrior warrior = new Warrior(username, 1, 1, 4, 4, "Warrior", 1);
                    return warrior;

                case "RO":
                    Rogue rogue = new Rogue(username, 1, 1, 4, 4, "Rogue", 1);
                    return rogue;
            }

            return CreateCharacter(); 
        }

      

        static int indexMainMenu = 0;

        /// <summary>
        /// Creates a user interface
        /// </summary>
        /// <param name="c"></param>
        public static void MainMenu(Character c)
        {
            Console.Clear();


            List<string> menuItems = new List<string>()
            {
                "Inventory",
                "Player Statistics",
                "Shop",
                "Level up",
                "Exit"
            };

            Console.CursorVisible = false;
            while (true)
            {

                Console.WriteLine("\nWelcome: " + c.Name);

                string selectedMenuItem = drawMainMenu(menuItems);
                if (selectedMenuItem == "Inventory")
                {
                    Console.Clear();
                    drawInventory(c);
                }

                else if (selectedMenuItem == "Player Statistics")
                {
                    Console.Clear();
                    DisplayProfileStats(c);
                }

                else if (selectedMenuItem == "Shop")
                {
                    //generate Items
                    Console.Clear();
                    drawShop(c);
                    string choice = Console.ReadLine();
                }

                else if (selectedMenuItem == "Level up")
                {
                    Console.Clear();
                    c.LevelUp();

                }

                else if (selectedMenuItem == "Exit")
                {
                    Environment.Exit(0);
                }
            }
        }

        /// <summary>
        /// Displaying the profile starts of current character
        /// </summary>
        /// <param name="c"></param>

        private static void DisplayProfileStats(Character c)
        {
            Console.WriteLine("Username: " + c.Name);
            Console.WriteLine("Level: " + c.Level);
            Console.WriteLine("Strength: " + c.Strength);
            Console.WriteLine("Dexterity: " + c.Dexterity);
            Console.WriteLine("Intelligence: " + c.Intelligence);
            Console.WriteLine("Character class: " + c.CharacterType);
            Console.WriteLine("Character damage per second: " + c.Damage);
        }


            /// <summary>
            /// Displaying the menu list
            /// </summary>
            /// <param name="items"></param>
            /// <returns></returns>

            public static string drawMainMenu(List<string> items)
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (i == indexMainMenu)
                {
                    Console.BackgroundColor = ConsoleColor.Gray;
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.WriteLine(items[i]);
                }
                else
                {
                    Console.WriteLine(items[i]);
                }
                Console.ResetColor();
            }

            ConsoleKeyInfo ckey = Console.ReadKey();
            if (ckey.Key == ConsoleKey.DownArrow)
            {
                if (indexMainMenu == items.Count - 1) { }
                else { indexMainMenu++; }
            }
            else if (ckey.Key == ConsoleKey.UpArrow)
            {
                if (indexMainMenu <= 0) { }
                else { indexMainMenu--; }
            }
            else if (ckey.Key == ConsoleKey.LeftArrow)
            {
                Console.Clear();
            }
            else if (ckey.Key == ConsoleKey.RightArrow)
            {
                Console.Clear();
            }
            else if (ckey.Key == ConsoleKey.Enter)
            {
                return items[indexMainMenu];
            }
            else
            {
                return "";
            }

            Console.Clear();
            return "";
        }

        /// <summary>
        /// Displaying the current equipped Items
        /// </summary>
        /// <param name="character"></param>

        public static void drawInventory(Character character) {


            foreach (KeyValuePair<SlotsEnums, Item> item in character.Equipment)
            {
                if (item.Value != null)
                {
                    Console.WriteLine("Item name: " + item.Value.Name + "\nItem level required: " + item.Value.Level.ToString());
                }
                else
                {
                    Console.WriteLine("Empty slot");
                }

            }

        }


        /// <summary>
        /// Displaying available items to equip/buy
        /// </summary>
        /// <param name="c"></param>

        public static void drawShop(Character c) {

            Dictionary<int, Weapons> weapons = new Dictionary<int, Weapons>()
            {
                { 0, new Weapons("Axe", 1, SlotsEnums.Weapon, WeaponsEnums.Axe, 5, 2, 1, 1) },
                { 1, new Weapons("Staff", 1, SlotsEnums.Weapon, WeaponsEnums.Staff, 1, 2, 5, 2) },
                { 2, new Weapons("Bow", 1, SlotsEnums.Weapon, WeaponsEnums.Bow, 1, 5, 2, 3) },

                { 3, new Weapons("Scimitar", 2, SlotsEnums.Weapon, WeaponsEnums.Sword, 8, 4, 1, 4) },
                { 4, new Weapons("Super staff", 2, SlotsEnums.Weapon, WeaponsEnums.Wand, 1, 4, 8, 3) },
                { 5, new Weapons("Crystal bow", 2, SlotsEnums.Weapon, WeaponsEnums.Dagger, 1, 8, 4, 5) }
            };

            Dictionary<int, Armor> armor = new Dictionary<int, Armor>()
            {
                { 6, new Armor("Bronze Helmet", 1, SlotsEnums.Head, ArmorEnums.Mail,2, 2, 5) },
                { 7, new Armor("Wizard Robe Top", 1, SlotsEnums.Body, ArmorEnums.Cloth , 1, 2, 5) },
                { 8, new Armor("Leather Pants", 1, SlotsEnums.Legs, ArmorEnums.Leather, 1, 5, 2) },

                { 9, new Armor("Golden Helmet", 2, SlotsEnums.Head, ArmorEnums.Plate, 4, 4, 4) },
                { 10, new Armor("Wizard Flying Top", 2, SlotsEnums.Body, ArmorEnums.Cloth , 4, 1, 6) },
                { 11, new Armor("Crystal Pants", 2, SlotsEnums.Legs, ArmorEnums.Leather, 3, 8, 2) }
            };

            Console.WriteLine("------------------------------------------------");
            Console.WriteLine("ALL WEAPON ITEMS");
            Console.WriteLine("------------------------------------------------");
            foreach (KeyValuePair<int, Weapons> weaponItem in weapons)
            {
                Console.WriteLine("Type this number to equip: " + weaponItem.Key + "\n");
                Console.WriteLine("Name: " + weaponItem.Value.Name + "\n");
                Console.WriteLine("Required level: " + weaponItem.Value.Level + "\n");
                Console.WriteLine("Damage: " + weaponItem.Value.Damage + "\n");
                Console.WriteLine("------------------------------------------------");
            }

            Console.WriteLine("ALL ARMOR ITEMS");

            Console.WriteLine("------------------------------------------------");
            foreach (KeyValuePair<int, Armor> armorItem in armor)
            {
                Console.WriteLine("Type this number to equip: " + armorItem.Key + "\n");
                Console.WriteLine("Name: " + armorItem.Value.Name + "\n");
                Console.WriteLine("Required level: " + armorItem.Value.Level + "\n");
                Console.WriteLine("------------------------------------------------");
            }
            EquipWeapon(c, weapons);
            EquipArmor(c, armor);
            UpdateCharacter(c);
        }


        /// <summary>
        /// Updating Character stats based on character class
        /// </summary>
        /// <param name="c"></param>
        public static void UpdateCharacter(Character c)
        {
            double ratio = 0.01;

            if (c.CharacterType == "Mage")
            {
                c.Damage = c.Damage + (c.Intelligence * ratio);
            }

            if (c.CharacterType == "Ranger")
            {
                c.Damage = c.Damage + (c.Dexterity * ratio);

            }

            if (c.CharacterType == "Rogue")
            {
                c.Damage = c.Damage + (c.Dexterity * ratio);
            }

            if (c.CharacterType == "Warrior")
            {
                c.Damage = c.Damage + (c.Strength * ratio);
            }
        }


        /// <summary>
        /// Equip an available Weapon and get the bonus attributes of it
        /// </summary>
        /// <param name="c"></param>
        /// <param name="weapons"></param>
        public static void EquipWeapon(Character c, Dictionary<int, Weapons> weapons)
        {
            Console.WriteLine("Choose which weapon to equip: ");
            string choice = Console.ReadLine();

            foreach (KeyValuePair<int, Weapons> weaponItem in weapons)
            {
                if (choice == weaponItem.Key.ToString())
                {
                    if (weaponItem.Value.Level > c.Level || !c.AllowedWeapons.Contains(weaponItem.Value.WeaponsType))
                    {
                        throw new InvalidWeaponException("Level is too low or character type doesnt suit the weapon");
                    }

                    c.Strength = c.Strength + weaponItem.Value.Strength;
                    c.Dexterity = c.Dexterity + weaponItem.Value.Dexterity;
                    c.Intelligence = c.Intelligence + weaponItem.Value.Intelligence;

                    c.EquipWeapons(weaponItem.Value);
                }
            }
        }

        /// <summary>
        /// Equip armor and get bonus attributes 
        /// </summary>
        /// <param name="c"></param>
        /// <param name="armor"></param>
        /// <exception cref="InvalidArmorException"></exception>
     
        public static void EquipArmor(Character c, Dictionary<int, Armor> armor)
        {
            Console.WriteLine("Choose which Armor to equip: ");
            string choice = Console.ReadLine();

            foreach (KeyValuePair<int, Armor> armorItem in armor)
            {
                if (choice == armorItem.Key.ToString())
                {
                    if (armorItem.Value.Level > c.Level || !c.AllowedArmor.Contains(armorItem.Value.ArmorType))
                    {
                        throw new InvalidArmorException("Level is too low or character type doesnt suit the armor item");
                    }

                    c.EquipArmor(armorItem.Value);
                }
            }          
        }
    }
}
