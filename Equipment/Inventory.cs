﻿using RPG_CONSOLE_APP.Equipment;
using RPG_CONSOLE_APP.Equipment.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_CONSOLE_APP
{
    public class Inventory
    {
        public  Dictionary<int, Weapons> ItemsInventory { get; set; }

        public Inventory(int size)
        {
            InitializeInventory(size);
        }

        /// <summary>
        /// Initalizing an inventory of specific size
        /// </summary>
        /// <param name="size"></param>
        private void InitializeInventory(int size)
        {
            ItemsInventory = new Dictionary<int, Weapons>();   
            for (int i = 1; i <= size; i++)
            {
                ItemsInventory.Add(i, null);
            }

        }  

        /// <summary>
        /// Get the size of inventory
        /// </summary>
        /// <returns></returns>
        public int GetInventorySize()
        {
            return ItemsInventory.Count;    
        }

    }
}
