﻿using System.Collections.Generic;

namespace RPG_CONSOLE_APP
{
    public abstract class Item
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public SlotsEnums Slot { get; set; }
        

        public Item(string name, int level, SlotsEnums slot)
        {
            Name = name;
            Level = level;
            Slot = slot;
        }     
    }
}