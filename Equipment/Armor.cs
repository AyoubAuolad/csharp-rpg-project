﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_CONSOLE_APP.Equipment
{
    public class Armor : Item
    {
        public ArmorEnums ArmorType { get; set; }
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        public Armor(string name, int level, SlotsEnums slot, ArmorEnums armortype, int strength, int dexterity, int intelligence ) : base(name, level, slot)
        {
            ArmorType = armortype;
            Strength = strength;    
            Dexterity = dexterity;
            Intelligence = intelligence;    
        }
    }
}
