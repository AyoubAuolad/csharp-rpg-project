﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_CONSOLE_APP.Equipment
{

    public class Weapons : Item
    {
        public WeaponsEnums WeaponsType { get; set; }
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public int Damage { get; set; }
        public int AttackSpeed { get; set; }


        public Weapons(string name, int level, SlotsEnums slot, WeaponsEnums weaponstype, int strength, int dexterity, int intelligence, int attackspeed) : base(name, level, slot)
        {
            Name = name;
            Level = level;
            Slot = slot;
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
            AttackSpeed = attackspeed;
            WeaponsType = weaponstype;
            Damage = Strength + Dexterity + Intelligence;
        }
    }
}
