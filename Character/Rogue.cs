﻿using RPG_CONSOLE_APP.Equipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_CONSOLE_APP
{
    public class Rogue : Character
    {
        public Rogue(string name, int level, int strength, int dexterity, int intelligence, string charactertype, double damage) : base(name, level, strength, dexterity, intelligence, charactertype, damage)
        {
            Name = name;
            Level = level;
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
            CharacterType = charactertype;
            double Bonusdamage = dexterity * 0.01;
            Damage = damage + Bonusdamage;

            AllowedWeapons.Add(WeaponsEnums.Dagger);
            AllowedWeapons.Add(WeaponsEnums.Sword);

            AllowedArmor.Add(ArmorEnums.Leather);
            AllowedArmor.Add(ArmorEnums.Mail);

        }
        public override int LevelUp()
        {
            Strength = Strength + 1;
            Dexterity = Dexterity + 4;
            Intelligence = Intelligence + 1;
            Level = Level + 1;

            Damage = Damage + (Dexterity * 0.01);

            return Level;
        }

    }
}
