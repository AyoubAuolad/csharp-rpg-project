﻿using RPG_CONSOLE_APP.Equipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_CONSOLE_APP
{
    public class Warrior : Character
    {
        public Warrior(string name, int level, int strength, int dexterity, int intelligence, string charactertype, double damage) : base(name, level, strength, dexterity, intelligence, charactertype, damage)
        {
            Name = name;
            Level = level;
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
            CharacterType = charactertype;
            double Bonusdamage = strength * 0.01;
            Damage = damage + Bonusdamage;

            AllowedWeapons.Add(WeaponsEnums.Axe);
            AllowedWeapons.Add(WeaponsEnums.Sword);
            AllowedWeapons.Add(WeaponsEnums.Hammer);

            AllowedArmor.Add(ArmorEnums.Mail);
            AllowedArmor.Add(ArmorEnums.Plate);
        }

        public override int LevelUp()
        {
            Strength = Strength + 3;
            Dexterity = Dexterity + 2;
            Intelligence = Intelligence + 1;
            Level = Level + 1;

            Damage = Damage + (Strength * 0.01);

            return Level;
        }

    }
}
