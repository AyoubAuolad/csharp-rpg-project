﻿using RPG_CONSOLE_APP.Equipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_CONSOLE_APP
{
    public abstract class Character
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public string CharacterType { get; set; }
        public double Damage { get; set; }
        public CharacterEnums characterEnums { get; set; }

        public readonly List<ArmorEnums> AllowedArmor = new List<ArmorEnums>();
        public readonly List<WeaponsEnums> AllowedWeapons = new List<WeaponsEnums>();


        public Dictionary<SlotsEnums, Item?> Equipment { get; set; } = new Dictionary<SlotsEnums, Item?>()
        {
            { SlotsEnums.Body, null },
            { SlotsEnums.Head, null },
            { SlotsEnums.Legs, null },
            { SlotsEnums.Weapon, null }
        };


        public Character(string name, int level, int strength, int dexterity, int intelligence, string charactertype, double damage)
        {
            Name = name;
            Level = level;
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
            CharacterType = charactertype;
            Damage = damage;


        }

        /// <summary>
        /// Levelup character and updates character attributes
        /// </summary>
        /// <returns></returns>
        public abstract int LevelUp();
        

        /// <summary>
        /// Equip a weapon type in weapon slot inside Item dictionairy
        /// </summary>
        /// <param name="weapon"></param>
        /// <returns></returns>
        public virtual string EquipWeapons(Weapons weapon)
        {
            Equipment[weapon.Slot] = weapon;

            return "New weapon equipped!";
        }

        /// <summary>
        /// Equip an armor type in armor slot inside the Item dictionairy
        /// </summary>
        /// <param name="armor"></param>
        /// <returns></returns>

        public virtual string EquipArmor(Armor armor)
        {

            Equipment[armor.Slot] = armor;

            return "New armor equipped!";

        }

        // ADD ITEMS TO INVENTORY







    }
}
