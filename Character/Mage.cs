﻿using RPG_CONSOLE_APP.Equipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_CONSOLE_APP
{
    public class Mage : Character
    {
        public Mage(string name, int level, int strength, int dexterity, int intelligence, string charactertype, double damage) : base(name, level, strength, dexterity, intelligence, charactertype, damage)
        {
            Name = name;
            Level = level;
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
            CharacterType = charactertype;
            Damage = damage;

            AllowedWeapons.Add(WeaponsEnums.Staff);
            AllowedWeapons.Add(WeaponsEnums.Wand);

            AllowedArmor.Add(ArmorEnums.Cloth);
        }

        public override int LevelUp()
        {
            Strength = Strength + 1; 
            Dexterity = Dexterity + 1;
            Intelligence = Intelligence + 5;
            Level = Level + 1;

            Damage = Damage + (Intelligence * 0.01);

            return Level;
        }


    }
    }
